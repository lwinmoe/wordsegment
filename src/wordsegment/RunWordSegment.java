package wordsegment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.File;

public class RunWordSegment {

	public RunWordSegment() { }
	public static ArrayList<String> readData(String file) {
		ArrayList<String> sentences = new ArrayList<String>();
		String sLine = "";
		BufferedReader br = null;
		try {
			//br = new BufferedReader(new FileReader("data/dict.txt"));
			br = new BufferedReader(new FileReader(file));
			while ((sLine = br.readLine()) != null)   {
				//System.out.println (sLine);
				sentences.add(sLine);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sentences;
	}
	public static void main(String[] args) throws Exception {
		/* 
		// TEST CASES 
		//String str = "botheggearthandsaturnspin";
		//String str = "ကကတစ်က ကတိုးကကုသန်";
		//String str = "nowthatcherisdead";
		
		WordSegment w = new WordSegment("data/eng");
		ArrayList<String> segments = new ArrayList<String>();
		segments = w.longestMatch(str);
		for (int k = 0; k < segments.size();k++) {
			System.out.println(segments.get(k) + " ");
		}
		// END TEST CASES
		*/

		// create stop words list
		StopWords stop_words = new StopWords("data/stopwords");
		
		// Directory path here
		String path = "data/"; 
		 
		String currFile;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles(); 
		// go through each file 
		for (int cnt = 0; cnt < listOfFiles.length; cnt++) {
			if (listOfFiles[cnt].isFile()) {
				currFile = listOfFiles[cnt].getName();
				if (currFile.endsWith(".txt") || currFile.endsWith(".TXT")) {
					String year = currFile.substring(0,4);
					System.out.println("Processing " + currFile + " (" + year + ")");
					String outFile = "data-" + year + ".txt";
					PrintWriter writer;
					writer = new PrintWriter(outFile, "UTF-8");
							
					ArrayList<String> sentences = new ArrayList<String>();
					
					//sentences = readData("data/2007_blr_utf8.txt");
					String dataFile = "data/" + currFile;
					sentences = readData(dataFile);
					for (int i=0; i<sentences.size();i++) {
						//writer.println(sentences.get(i));
						ArrayList<String> segments = new ArrayList<String>();
						
						WordSegment w = new WordSegment();
						String[] sentence = sentences.get(i).split(" ");
						for (int j=0;j<sentence.length;j++) {
							//System.out.println("========================");
							//System.out.println(sentence[i]);
							//System.out.println("========================");
							//segments = w.segment(sentences.get(i));
							segments = w.longestMatch(sentence[j]);
							for (int k = 0; k < segments.size();k++) {
								if (!stop_words.stop_words.contains(segments.get(k))) {
								  writer.print(segments.get(k) + " ");
								}
							}
						}
						if (!segments.isEmpty()) {
						    writer.println("\n");
						}	
					}
					writer.close();
				}
			}
		}		
		System.out.println("Done");
	}
	
}
