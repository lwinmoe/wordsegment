package wordsegment;

import java.util.ArrayList;
import java.util.HashMap;

public class WordSegment {
	Dictionary d;
	HashMap<String, String> dict = new HashMap<String, String>();	
	
	public WordSegment() {
		d = new Dictionary("data/burmese_head_words");
		dict = d.getDict();
		//System.out.println(dict.size());
	}
	
	public WordSegment(String dictFile) {
		d = new Dictionary(dictFile);
		dict = d.getDict();
	}
	
	public ArrayList<String> longestMatch(String str) {
		ArrayList<String> segments = new ArrayList<String>();
		//System.out.println(str);
		String word = "";
		String tmp = "";
		for (int i=0;i<str.length();i++) {		
			for (int j=str.length();j>i;j--) {
				word = str.substring(i,j);
				// System.out.println("current word: " + word + "(i:" + i + " and j:" + j);
						//print qq(Checking at $i and $j: $word\n);
						//print qq(    $word at $i and $j\n) if $words{$word};
				if (i == j-1 && !dict.containsKey(word)) {					
					tmp += word;
					// System.out.println(word + " at i:" + i + " and j:" + j + " => " + tmp);
				}

				if (dict.containsKey(word)) {
					if (tmp != "") {
						segments.add(tmp);
					}	
					tmp = "";
					segments.add(word);
					i = j-1;
					// System.out.println("i:" + i + " and j:" + j);					
				}
		  }
		}
		// last case
		if (tmp != "") {
			segments.add(tmp);
		}	
		tmp = "";
		return segments;
	}
}
