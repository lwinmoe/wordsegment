package wordsegment;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class StopWords {
	
	public static ArrayList<String> stop_words = new ArrayList<String>();

	public StopWords(String fileName) {
		addStopWords(fileName);		
	}
	
	private void addStopWords(String fileName) {
		BufferedReader br = null;		 
		try { 
			String currLine; 
			br = new BufferedReader(new FileReader(fileName));				
			while ((currLine = br.readLine()) != null) {
				//System.out.println(currLine);
				stop_words.add(currLine);
			} 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}