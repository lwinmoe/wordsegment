package wordsegment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Dictionary {
	HashMap<String, String> dict_hash = new HashMap<String, String>();

	public Dictionary(String fileName) {
		String sLine = "";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
			while ((sLine = br.readLine()) != null)   {
				//System.out.println (sLine);
				if (!sLine.contains("##")) {
				    dict_hash.put(sLine, sLine);
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public HashMap getDict() {
		return dict_hash;
	}
}
